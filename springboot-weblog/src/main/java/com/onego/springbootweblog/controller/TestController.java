package com.onego.springbootweblog.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TestController {

    @ApiOperation(value=" 测试es日志" , notes ="1")
    @RequestMapping(value = "/testes",method = {RequestMethod.POST})
    public String testEs() {
       return "测试es日志返回";
    }

    @ApiOperation(value=" 测试es日志" , notes ="1")
    @RequestMapping(value = "/testes2",method = {RequestMethod.POST})
    public String testEs2() {
        System.out.println(1/0);
        return "测试es日志返回";
    }
}
