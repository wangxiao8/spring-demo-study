package com.xiao.jdkproxy;

/**
 * @description: 卖票接口
 * @author: wangxiao
 * @create: 2024-05-07 07:47
 **/
public interface SellTickets {
    void sell();
}
