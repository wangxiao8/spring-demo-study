package com.xiao.staticproxy;

/**
 * @description: 代售点
 * @author: wangxiao
 * @create: 2024-05-08 07:15
 **/
public class ProxyPoint implements SellTickets {

    private TrainStation station = new TrainStation();

    public void sell() {
        System.out.println("代理点收取一些服务费用");
        station.sell();
    }
}
