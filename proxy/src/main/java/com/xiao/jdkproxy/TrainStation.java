package com.xiao.jdkproxy;

/**
 * @description: 火车站类
 * @author: wangxiao
 * @create: 2024-05-07 07:47
 **/
public class TrainStation implements SellTickets{
    @Override
    public void sell() {
        System.out.println("火车站卖票");
    }
}
