package com.xiao.jdkproxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @description: 获取代理类i对象的工厂工厂类
 * @author: wangxiao
 * @create: 2024-05-07 07:38
 **/
public class ProxyFactory {

    public TrainStation station=new TrainStation();

    public SellTickets getProxyObject() {
        //返回代理对象
        /*
         * ClassLoader loader, 类加载器，用于加载代理类。通过目标对象获取类加载器
         * Class<?>[] interfaces 代理类实现的接口的字节码对象
         *InvocationHandler h 代理对象的调用处理程序
         */
        SellTickets proxyObject = (SellTickets)Proxy.newProxyInstance(
                station.getClass().getClassLoader(),
                station.getClass().getInterfaces(),
                new InvocationHandler() {
                    /*
                    Object proxy：代理对象，和proxyObject是同一个对象
                     Method method：对接口中的方法进行封装的Method对象
                     Object[] args：调用方法的实际参数
                       返回值就是方法的返回值
                     */
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        System.out.println("代售点收取服务费（jdk动态代理）");
                        //执行目标对象的方法
                        Object invoke = method.invoke(station, args);
                        return invoke;
                    }
                }
        );

        return proxyObject;
    }
}
