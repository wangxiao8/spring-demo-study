package com.onego.springbootweblog.repository;

import com.onego.springbootweblog.entity.WebExceptionLog;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * @author wangyu
 * @date 2023-05-18 14:36
 */
public interface EsWebExceptionLogRepository extends ElasticsearchRepository<WebExceptionLog, Long> {
}
