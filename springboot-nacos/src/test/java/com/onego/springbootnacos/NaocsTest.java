package com.onego.springbootnacos;

import com.onego.springbootnacos.config.MinioConfig;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;

@SpringBootTest
@Component
public class NaocsTest {

    @Autowired
    private MinioConfig minioConfig;

    @Test
    public void test() {
        System.out.println(minioConfig.getAccessKey());
        System.out.println(minioConfig.getSecretKey());
        System.out.println(minioConfig.getIp());
        System.out.println(minioConfig.getPort());
    }
}
