package com.xiao.staticproxy;

/**
 * @description:
 * @author: wangxiao
 * @create: 2024-05-08 07:14
 **/
public interface SellTickets {
    void sell();
}
