# springboot-nacos

> 本 demo 主要演示了如何在 Spring Boot 中通过 nacos实现 Spring Boot 与Nacos 的集成，实现不同的环境隔离。


## bootstrap.properties

```yaml
#项目名称，和nacos中的配置文件名保持一致
spring.application.name=nacosdemo
#环境
spring.profiles.active=dev
#nacos地址
spring.cloud.nacos.server-addr=175.24.204.121:8848
#nacos配置文件后缀
spring.cloud.nacos.config.file-extension=yaml
#nacos用户名和密码
spring.cloud.nacos.username=nacosdemo
spring.cloud.nacos.password=nacosdemo
```

## 说明
Nacos版本为2.03
>在上面的配置中 使用的nacos配置文件名为nacosdemo-dev.yaml

>项目存在多个环境，如 prod，我们只需要在nacos中创建nacosdemo-prod.yaml，修改spring.profiles.active=prod既可切换环境

## pom.xml中引入以下依赖即可

```xml
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
            <version>2021.0.5.0</version>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-bootstrap</artifactId>
            <version>3.1.5</version>
        </dependency>
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-config</artifactId>
            <version>2021.0.5.0</version>
        </dependency>
```



