package com.onego.springbootweblog.entity;

import lombok.Data;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Setting;

import java.time.LocalDateTime;

/**
 * @author Administrator
 */
@Data
@ToString
@Document(indexName = "indexweblog")
@Setting(shards = 5)
public class WebLog {

    @Id
    private Long id;

    /**
     * 服务器ip
     */
    private String serverIp;

    /**
     * 服务器名称
     */
    private String serverHostName;

    /**
     * 操作描述
     */
    private String description;

    /**
     * 操作用户
     */
    private String username;

    /**
     * 消耗时间
     */
    private Integer spendTime;

    /**
     * 根路径
     */
    private String basePath;

    /**
     * URI
     */
    private String uri;

    /**
     * URL
     */
    private String url;

    /**
     * 请求类型
     */
    private String method;

    /**
     * IP地址
     */
    private String ip;

    /**
     * 请求参数
     */
    private String parameter;

    /**
     * 请求返回的结果
     */
    private String result;

    /**
     * 请求返回的结果太长时，截取部分展示
     */
    private String resultStr;

    /**
     * 客户端类型
     */
    private String clientType;

    /**
     * 客户端操作系统类型
     */
    private String osType;

    /**
     * 请求结果 0失败 1成功
     */
    private Integer successFlag;

    /**
     * 异常错误
     */
    private String exception;

    /**
     * 文档生成时间戳
     */
    private LocalDateTime localDateTime;

    /**
     * 更新时间
     */
    private String updateTime;

    /**
     * 创建时间
     */
    private String createTime;
}
