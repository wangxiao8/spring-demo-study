package com.xiao.staticproxy;

/**
 * @description: 客户端测试类
 * @author: wangxiao
 * @create: 2024-05-08 07:15
 **/
public class Client {
    public static void main(String[] args) {
        ProxyPoint pp = new ProxyPoint();
        pp.sell();
    }
}