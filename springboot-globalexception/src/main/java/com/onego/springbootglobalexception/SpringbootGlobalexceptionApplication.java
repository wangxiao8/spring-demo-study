package com.onego.springbootglobalexception;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootGlobalexceptionApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootGlobalexceptionApplication.class, args);
    }

}
