package com.xiao.jdkproxy;

/**
 * @description: 客户类
 * @author: wangxiao
 * @create: 2024-05-07 07:54
 **/
public class Client {
    public static void main(String[] args) {
        //获取代理工厂对象
        ProxyFactory proxyFactory = new ProxyFactory();
        SellTickets proxyObject = proxyFactory.getProxyObject();
        proxyObject.sell();

        System.out.println(proxyObject.getClass());

        while (true){}
    }
}
