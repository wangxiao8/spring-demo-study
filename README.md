# spring-demo-study

#### 介绍
一个基于springboot的各种案例练手的demo项目
+ 觉得有用帮忙点点Star ✨

#### 模块介绍

| Module 名称                                                  | Module 介绍                                                  |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| [springboot-nacos](./springboot-nacos)                       | springboot集成nacos配置中心,实现简单修改配置文件,切换不同环境环境的效果                              |
| [springboot-globalexception](./springboot-globalexception)   | springboot全局异常处理,实现统一异常处理,方便开发人员维护异常处理代码                              |

#### 项目趋势



<a href="https://star-history.com/#https:/&wxgitee1/spring-demo-study&Date">
  <picture>
    <source media="(prefers-color-scheme: dark)" srcset="https://api.star-history.com/svg?repos=https:/%2Cwxgitee1/spring-demo-study&type=Date&theme=dark" />
    <source media="(prefers-color-scheme: light)" srcset="https://api.star-history.com/svg?repos=https:/%2Cwxgitee1/spring-demo-study&type=Date" />
    <img alt="Star History Chart" src="https://api.star-history.com/svg?repos=https:/%2Cwxgitee1/spring-demo-study&type=Date" />
  </picture>
</a>
