package com.onego.springbootweblog.entity;

import lombok.Data;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Setting;

import java.time.LocalDateTime;

/**
 * @author Administrator
 */
@Data
@ToString
@Document(indexName = "indexexceptionlog")
@Setting(shards = 5)
public class WebExceptionLog {

    @Id
    private Long id;

    /**
     * 服务器ip
     */
    private String serverIp;

    /**
     * 服务器名称
     */
    private String serverHostName;

    /**
     * 操作用户
     */
    private String username;

    /**
     * 根路径
     */
    private String basePath;

    /**
     * URI
     */
    private String uri;

    /**
     * URL
     */
    private String url;

    /**
     * 请求类型
     */
    private String method;

    /**
     * IP地址
     */
    private String ip;

    /**
     * 客户端类型
     */
    private String clientType;

    /**
     * 客户端操作系统类型
     */
    private String osType;

    /**
     * 异常错误
     */
    private String exception;

    /**
     * 文档生成时间戳
     */
    private LocalDateTime localDateTime;

    /**
     * 更新时间
     */
    private String updateTime;

    /**
     * 创建时间
     */
    private String createTime;
}
