package com.xiao.cglibproxy;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @description: 代理对象工厂，用来获取代理对象
 * @author: wangxiao
 * @create: 2024-05-08 07:01
 **/
public class ProxyFactory  implements MethodInterceptor {

    //
    private TrainStation trainStation=new TrainStation();

    public TrainStation getProxyObject() {
        //创建Enhancer对象,相当于Jdk中的Proxy
        Enhancer enhancer = new Enhancer();
        //设置父类的字节码对象
        enhancer.setSuperclass(TrainStation.class);
        //设计回调函数
        enhancer.setCallback(this);
        //创建代理对象
        TrainStation proxyObject =(TrainStation) enhancer.create();

        return proxyObject;
    }

    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        System.out.println("cglib代理开始");
        System.out.println("cglib收取服务费");
        //调用目标对象方法
        Object obj = method.invoke(trainStation, objects);
        return obj;
    }
}
