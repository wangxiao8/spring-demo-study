package com.xiao.cglibproxy;

/**
 * @description: 客户端
 * @author: wangxiao
 * @create: 2024-05-08 07:07
 **/
public class Client {
    public static void main(String[] args) {
        ProxyFactory proxyFactory = new ProxyFactory();
        TrainStation proxyObject = proxyFactory.getProxyObject();
        //执行代理对象的sell方法
        proxyObject.sell();
    }
}
