package com.onego.springbootweblog.config;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.core.util.URLUtil;
import com.alibaba.fastjson2.JSON;
import com.onego.springbootweblog.entity.WebExceptionLog;
import com.onego.springbootweblog.entity.WebLog;
import com.onego.springbootweblog.repository.EsWebExceptionLogRepository;
import com.onego.springbootweblog.repository.EsWebLogRepository;
import eu.bitwalker.useragentutils.UserAgent;
import io.swagger.annotations.ApiOperation;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;


import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author wangyu
 * @date 2023-05-09 10:20
 */
@Aspect
@Component
@Order(1)
public class WebLogAspect {

    private static final Logger logger = LoggerFactory.getLogger(WebLogAspect.class);



    @Resource
    private EsWebLogRepository esWebLogRepository;

    @Resource
    private EsWebExceptionLogRepository esWebExceptionLogRepository;

    /**
     * 线程池
     */
    private ThreadPoolTaskExecutor taskExecutor;

    private Snowflake snowflake;

    private String serverIp;

    private String serverHostName;

    public WebLogAspect() {
        this.initThread();
    }

    @Pointcut("execution(* com.onego.springbootweblog.controller..*(..))")
    public void webLog() {
    }

    @AfterThrowing(pointcut = "webLog()", throwing = "e")
    public void doAfterThrow(JoinPoint joinPoint, Exception e) {
        try {
            String now = DateUtil.formatDateTime(new Date());
            ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            if (null != attributes) {
                HttpServletRequest request = attributes.getRequest();
                WebLog webLog = this.buildWebLog(request, e);
                if (null != webLog) {
                    Signature signature = joinPoint.getSignature();
                    MethodSignature methodSignature = (MethodSignature) signature;
                    Method method = methodSignature.getMethod();
                    if (method.isAnnotationPresent(ApiOperation.class)) {
                        ApiOperation apiOperation = method.getAnnotation(ApiOperation.class);
                        webLog.setDescription(apiOperation.value());
                    }
                    webLog.setParameter(JSON.toJSONString(this.getRequestParamsByJoinPoint(joinPoint)));
                    webLog.setResult("");
                    webLog.setResultStr("");
                    webLog.setSpendTime(0);
                    webLog.setCreateTime(now);
                    webLog.setUpdateTime(now);
                    this.submitLog(webLog);
                }
            }
        } catch (Exception exc) {
            logger.error("doAfterThrow:Exception:"+exc.getMessage());
        }
    }

    @Around("webLog()")
    public Object doAround(ProceedingJoinPoint joinPoint) throws Throwable {
        String now = DateUtil.formatDateTime(new Date());
        long startTime = System.currentTimeMillis();
        Object result = joinPoint.proceed();
        try {
            //获取当前请求对象
            ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            if(null != attributes) {
                HttpServletRequest request = attributes.getRequest();
                WebLog webLog = this.buildWebLog(request, null);
                if (null != webLog) {
                    //记录请求信息
                    Signature signature = joinPoint.getSignature();
                    MethodSignature methodSignature = (MethodSignature) signature;
                    Method method = methodSignature.getMethod();
                    if (method.isAnnotationPresent(ApiOperation.class)) {
                        ApiOperation apiOperation = method.getAnnotation(ApiOperation.class);
                        webLog.setDescription(apiOperation.value());
                    }
                    webLog.setParameter(JSON.toJSONString(this.getRequestParamsByProceedingJoinPoint(joinPoint)));
                    if (null != result) {
                        if (JSON.toJSONString(result).length() > 500) {
                            webLog.setResult("");
                            webLog.setResultStr(JSON.toJSONString(result).substring(0, 500));
                        } else {
                            webLog.setResult(JSON.toJSONString(result));
                            webLog.setResultStr("");
                        }
                    } else {
                        webLog.setResult("");
                        webLog.setResultStr("");
                    }
                    long endTime = System.currentTimeMillis();
                    webLog.setSpendTime((int) (endTime - startTime));
                    webLog.setCreateTime(now);
                    webLog.setUpdateTime(now);
                    this.submitLog(webLog);
                }
            }
        } catch (Exception exc) {
            logger.error("doAround:Exception:"+exc.getMessage());
        }
        return result;
    }

    /**
     * 初始化线程池
     */
    private void initThread() {
        taskExecutor = new ThreadPoolTaskExecutor();
        //线程初始化
        taskExecutor.initialize();
        // 设置核心线程数
        int corePoolSize = 4;
        taskExecutor.setCorePoolSize(corePoolSize);
        // 设置最大线程数
        taskExecutor.setMaxPoolSize(corePoolSize * 2);
        // 设置队列容量
        taskExecutor.setQueueCapacity(1000);
        // 设置线程活跃时间（秒）
        taskExecutor.setKeepAliveSeconds(60);
        // 设置默认线程名称
        taskExecutor.setThreadNamePrefix("store-logs");
        // 设置拒绝策略
        taskExecutor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        // 等待所有任务结束后再关闭线程池
        taskExecutor.setWaitForTasksToCompleteOnShutdown(true);
        //雪花id
        snowflake = IdUtil.getSnowflake(12,12);
        try {
            InetAddress local = InetAddress.getLocalHost();
            serverIp = local.getHostAddress();
            serverHostName = local.getHostName();
        } catch (UnknownHostException e) {
            serverIp = "";
            serverHostName = "";
        }
    }

    private Map<String, Object> getRequestParamsByProceedingJoinPoint(ProceedingJoinPoint proceedingJoinPoint) {
        //参数名
        String[] paramNames = ((MethodSignature)proceedingJoinPoint.getSignature()).getParameterNames();
        //参数值
        Object[] paramValues = proceedingJoinPoint.getArgs();
        return buildRequestParam(paramNames, paramValues);
    }

    private Map<String, Object> getRequestParamsByJoinPoint(JoinPoint joinPoint) {
        //参数名
        String[] paramNames = ((MethodSignature)joinPoint.getSignature()).getParameterNames();
        //参数值
        Object[] paramValues = joinPoint.getArgs();
        return buildRequestParam(paramNames, paramValues);
    }

    private Map<String, Object> buildRequestParam(String[] paramNames, Object[] paramValues) {
        Map<String, Object> requestParams = new HashMap<>(16);
        for (int i = 0; i < paramNames.length; i++) {
            Object value = paramValues[i];
            //如果是文件对象
            if (value instanceof MultipartFile) {
                MultipartFile file = (MultipartFile) value;
                //获取文件名
                value = file.getOriginalFilename();
            }
            requestParams.put(paramNames[i], value);
        }
        requestParams.remove("request");
        requestParams.remove("response");
        return requestParams;
    }

    /**
     * 构建日志信息
     */
    private WebLog buildWebLog(HttpServletRequest request, Exception e) {
        try {
            WebLog webLog = new WebLog();
            webLog.setId(snowflake.nextId());
            webLog.setServerIp(serverIp);
            webLog.setServerHostName(serverHostName);
            String urlStr = request.getRequestURL().toString();
            webLog.setBasePath(StrUtil.removeSuffix(urlStr, URLUtil.url(urlStr).getPath()));
            webLog.setIp(request.getRemoteUser());
            webLog.setMethod(request.getMethod());
            webLog.setUri(request.getRequestURI());
            webLog.setUrl(request.getRequestURL().toString());
            UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeader("user-agent"));
            webLog.setClientType(userAgent.getOperatingSystem().getDeviceType().getName());
            webLog.setOsType(userAgent.getOperatingSystem().getName());
            if (null == e) {
                webLog.setSuccessFlag(1);
                webLog.setException("");
            } else  {
                webLog.setSuccessFlag(0);
                webLog.setException(getStackTrace(e));
            }
            webLog.setLocalDateTime(LocalDateTime.now());
            return webLog;
        } catch (Exception exc) {
            return null;
        }
    }

    private String getStackTrace(Throwable throwable) {
        StringWriter sw = new StringWriter();
        try (PrintWriter pw = new PrintWriter(sw)) {
            throwable.printStackTrace(pw);
            return sw.toString();
        }
    }

    /**
     * 保存日志信息
     */
    private void submitLog(WebLog webLog) {
        taskExecutor.execute(() -> {
            try {
                if (null != webLog) {
                    esWebLogRepository.save(webLog);
                }
            } catch (Exception e) {
                if (!e.getMessage().contains("201 Created")) {
                    logger.error("WebLogAspect:submitLog:"+e.getMessage());
                    logger.error(webLog.toString());
                }
            }
        });
    }

    public void submitExceptionLog(WebExceptionLog webExceptionLog) {
        taskExecutor.execute(() -> {
            try {
                if (null != webExceptionLog) {
                    webExceptionLog.setId(snowflake.nextId());
                    webExceptionLog.setServerIp(serverIp);
                    webExceptionLog.setServerHostName(serverHostName);
                    webExceptionLog.setLocalDateTime(LocalDateTime.now());
                    esWebExceptionLogRepository.save(webExceptionLog);
                }
            } catch (Exception exc) {
                if (!exc.getMessage().contains("201 Created")) {
                    logger.error("WebLogAspect:submitExceptionLog:"+exc.getMessage());
                    logger.error(webExceptionLog.toString());
                }
            }
        });
    }
}
