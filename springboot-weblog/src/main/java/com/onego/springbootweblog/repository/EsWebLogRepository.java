package com.onego.springbootweblog.repository;


import com.onego.springbootweblog.entity.WebLog;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * @author wangyu
 * @date 2023-05-18 14:36
 */
public interface EsWebLogRepository extends ElasticsearchRepository<WebLog, Long> {
}
